<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php

    echo "<h3> Soal No 1 Greetings </h3>";

    function greetings($nama)
    {   
        echo "Halo $nama, Selamat Datang di Sanbercode!<br>";
    }

    greetings("Bagas");
    greetings("Wahyu");
    greetings("Abdul");

    echo "<br>";

    echo "<h3>Soal No 2 Reverse String</h3>";

    function reverseString($string)
    {
        $reverse = "";
        for($i = 1; $i <= strlen($string); $i++)
        {
            $reverse[$i] = substr($string, strlen($string)-$i, 1); 
        }
        print_r($reverse);
        echo "<br>";
    }

    reverseString("abdul");

    echo "<br>";

    echo "<h3>Soal No 3 Palindrome </h3>";

    function palindrome($kata)
    {   
        $reverse2 = "";
        for($a = 0; $a < strlen($kata)-1; $a++)
        {
            $reverse2[$a] = substr($kata, (strlen($kata)-1)-$a, 1);
        }
        $periksa = strcmp($kata, $reverse2);
        if($periksa != 1)
        {
            echo $kata.": false<br>";
        }
        else
        {
            echo $kata.": true<br>";
        }
        echo "<br>";
    }

    palindrome("civic") ;
    palindrome("nababan") ;
    palindrome("jambaban");
    palindrome("racecar");


    echo "<h3>Soal No 4 Tentukan Nilai </h3>";

    function tentukan_nilai($int)
    {
        if($int >= 85 && $int <= 100)
        {
            echo "Sangat Baik<br>";
        }
        elseif($int >= 70 && $int < 85)
        {
            echo "Baik<br>";
        }
        elseif($int >= 60 && $int < 70)
        {
            echo "Cukup<br>";
        }
        else
        {
            echo "Kurang<br>";
        }
    }

    echo tentukan_nilai(98);
    echo tentukan_nilai(76);
    echo tentukan_nilai(67);
    echo tentukan_nilai(43);

    ?>

</body>

</html>