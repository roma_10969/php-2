<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        echo "<h3>Soal No 1 Looping I Love PHP</h3>";
        
        echo "<h4>LOOPING PERTAMA</h4>";
        for ($loop1 = 2; $loop1 <= 20; $loop1+=2)
        {
            echo $loop1 . " - I Love PHP<br>";
        }
        echo "<h4>LOOPING KEDUA</h4>";
        for ($loop2 = 20; $loop2 >= 2; $loop2 -=2)
        {
            echo $loop2 . " - I Love PHP<br>";
        }

        echo "<h3>Soal No 2 Looping Array Modulo </h3>";

        $numbers = [18, 45, 29, 61, 47, 34];
        echo "Array numbers: ";
        print_r($numbers);
        
        foreach($numbers as $num)
        {
            $rest[] = $num + 5;
        }

        $rest = [];
        for($i = 0; $i < count($numbers); $i++)
        {
            $rest[$i] = $numbers[$i]+5;
        }
        echo "<br>";
        echo "Array tambah 5 adalah:  "; 
        print_r($rest);
        echo "<br>";

        echo "<h3> Soal No 3 Looping Asociative Array </h3>";

        $items = [
            ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
            ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
            ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
            ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
        ];
        
        foreach($items as $key => $val)
        {
            $items = array(
                'id' => $val[0],
                'name' => $val[1],
                'price' => $val[2],
                'description' => $val[3],
                'source' => $val[4],
            );
            print_r($items);
            echo "<br>";
        }
       
        echo "<h3>Soal No 4 Asterix </h3>";
        
        echo "Asterix: ";
        echo "<br>";      
        
        for($a = 1; $a <= 5; $a++)
        {
            for($b = 1; $b <= $a; $b++)
            {
                echo "* ";
            }
            echo "<br>";
        }
    ?>

</body>
</html>